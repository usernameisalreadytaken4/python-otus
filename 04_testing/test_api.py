# -*- coding: utf-8 -*-
import datetime
import hashlib
import pytest
import memcache
import redis
from pytest_mock import mocker

import api


class MemDict(dict):

    def __init__(self, *args, **kwargs):
        super(MemDict, self).__init__(*args, **kwargs)

    def set(self, key, value, time=None):
        self.update({key: value})


class RedisDict(dict):

    def __init__(self, *args, **kwargs):
        super(RedisDict, self).__init__(*args, **kwargs)

    def set(self, key, value):
        self.update({key: value})


@pytest.fixture()
def mocked_memcache(mocker):
    mocked_memcache = mocker.patch('memcache.Client')
    mocked_memcache.return_value = MemDict()
    return mocked_memcache

@pytest.fixture()
def mocked_fail_memcache(mocker):
    mocked_memcache = mocker.patch('memcache.Client')
    mocked_memcache.return_value = None
    return mocked_memcache


@pytest.fixture()
def mocked_redis(mocker):
    mocked_redis = mocker.patch('redis.StrictRedis')
    mocked_redis.return_value = RedisDict()
    return mocked_redis


class TestMeta(object):
    """
    Абстрактный класс, предоставляющий методы для предоставления информации по тестированию
    """
    def setup_class(self):
        self.context = {}
        self.headers = {}

    def get_response(self, request):
        return api.method_handler({"body": request, "headers": self.headers}, self.context)

    def set_valid_auth(self, request):
        if request.get("login") == api.ADMIN_LOGIN:
            request["token"] = hashlib.sha512(datetime.datetime.now().strftime("%Y%m%d%H") + api.ADMIN_SALT).hexdigest()
        else:
            msg = request.get("account", "") + request.get("login", "") + api.SALT
            request["token"] = hashlib.sha512(msg).hexdigest()


class TestConnection(TestMeta):
    """
    тестируем подключение, как с верными данными, так и с ложными
    """
    def test_empty_request(self):
        _, code = self.get_response({})
        assert api.INVALID_REQUEST == code

    @pytest.mark.parametrize('request', [
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "", "arguments": {}},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "sdd", "arguments": {}},
        {"account": "horns&hoofs", "login": "admin", "method": "online_score", "token": "", "arguments": {}},
    ])
    def test_bad_auth(self, request):
        _, code = self.get_response(request)
        assert api.FORBIDDEN == code

    @pytest.mark.parametrize('request', [
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score"},
        {"account": "horns&hoofs", "login": "h&f", "arguments": {}},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "arguments": {}},
    ])
    def test_invalid_method_request(self, request):
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.INVALID_REQUEST == code
        assert len(response) > 0


class TestScoring(TestMeta):
    """
    Тестируем подсчет, и кэширование подсчета
    """
    @pytest.mark.parametrize('arguments', [
        {},
        {"phone": "79175002040"},
        {"phone": "89175002040", "email": "stupnikov@otus.ru"},
        {"phone": "79175002040", "email": "stupnikovotus.ru"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": -1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": "1"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.1890"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "XXX"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000", "first_name": 1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
         "first_name": "s", "last_name": 2},
        {"phone": "79175002040", "birthday": "01.01.2000", "first_name": "s"},
        {"email": "stupnikov@otus.ru", "gender": 1, "last_name": 2},
    ])
    def test_invalid_score_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.INVALID_REQUEST == code
        assert len(response) > 0

    @pytest.mark.parametrize('arguments', [
        {"phone": "79175002040", "email": "stupnikov@otus.ru"},
        {"phone": 79175002040, "email": "stupnikov@otus.ru"},
        {"gender": 1, "birthday": "01.01.2000", "first_name": "a", "last_name": "b"},
         {"gender": 0, "birthday": "01.01.2000"},
        {"gender": 2, "birthday": "01.01.2000"},
        {"first_name": "a", "last_name": "b"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
         "first_name": "a", "last_name": "b"},
    ])
    def test_ok_score_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.OK == code
        score = response.get("score")
        assert (isinstance(score, (int, float)) and score >= 0, arguments)
        assert sorted(self.context["has"]) == sorted(arguments.keys())


class TestInterests(TestMeta):
    """
    тестируем интересы и кэширование интересов
    """

    @pytest.mark.parametrize('arguments', [
         {},
        {"date": "20.07.2017"},
        {"client_ids": [], "date": "20.07.2017"},
        {"client_ids": {1: 2}, "date": "20.07.2017"},
        {"client_ids": ["1", "2"], "date": "20.07.2017"},
         {"client_ids": [1, 2], "date": "XXX"},
    ])
    def test_invalid_interests_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "clients_interests", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.INVALID_REQUEST == code
        assert len(response)

    @pytest.mark.parametrize('arguments', [
        {"client_ids": [1, 2, 3], "date": datetime.datetime.today().strftime("%d.%m.%Y")},
        {"client_ids": [0]},
        {"client_ids": [1, 2], "date": "19.07.2017"},
    ])
    def test_ok_interests_request(self, arguments, mocked_memcache, mocked_redis):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "clients_interests", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.OK == code
        assert len(arguments["client_ids"]) == len(response['interests'])
        assert (all(v and isinstance(v, list) and all(isinstance(i, basestring) for i in v)
                        for v in response['interests'].values()))
        assert len(self.context.get('has').get("client_ids")) == len(arguments["client_ids"])\


    @pytest.mark.parametrize('arguments', [
        {"client_ids": [1, 2, 3], "date": datetime.datetime.today().strftime("%d.%m.%Y")},
          {"client_ids": [1, 2], "date": "19.07.2017"},
          {"client_ids": [0]},
    ])
    def test_ok_interests_request_without_memcache(self, arguments, mocked_fail_memcache, mocked_redis):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "clients_interests", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        assert api.OK == code
        assert len(arguments["client_ids"]) == len(response['interests'])
        assert (all(v and isinstance(v, list) and all(isinstance(i, basestring) for i in v)
                        for v in response['interests'].values()))
        assert len(self.context.get('has').get("client_ids")) == len(arguments["client_ids"])