# -*- coding: utf-8 -*-
import memcache
import redis
from time import sleep


MEMCACHE_ADDRESS = ['127.0.0.1:11211']
REDIS_ADDRESS = {
    'host': 'localhost',
    'port': 6379,
    'db': 0,
    'socket_timeout': 2,
    'socket_connect_timeout': 2
}


class Cache:
    """
    Singleton для кэша.
    mc - memcache с ограниченным временем хранения
    redis - персистентное хранилище
    """

    MAX_ATTEMPT = 5
    ATTEMPT_COUNT = 0
    BACKOFF_FACTOR = 0.01
    DELAY = 0

    def __init__(self, state=True):
        self.state = state
        self.mc = memcache.Client(MEMCACHE_ADDRESS)
        self.redis = redis.StrictRedis(**REDIS_ADDRESS)

    def get(self, key):
        """
        обращение напрямую к хранилищу
        """
        for attempt in xrange(1, Cache.MAX_ATTEMPT):
            sleep(Cache.DELAY)
            try:
                value = self.mc.get(key)
                if value:
                    Cache.DELAY = 0
                    return value
            except:
                Cache.ATTEMPT_COUNT += 1
                Cache.DELAY = Cache.BACKOFF_FACTOR * (2 ** Cache.ATTEMPT_COUNT)
        Cache.DELAY = 0
        Cache.ATTEMPT_COUNT = 1

    def set(self, key, value, time):
        for attempt in xrange(1, Cache.MAX_ATTEMPT):
            sleep(Cache.DELAY)
            try:
                self.mc.set(key, value, time)
            except:
                Cache.ATTEMPT_COUNT += 1
                Cache.DELAY = Cache.BACKOFF_FACTOR * (2 ** Cache.ATTEMPT_COUNT)
        Cache.DELAY = 0
        Cache.ATTEMPT_COUNT = 1

    def cache_set(self, key, value, time=None):
        """
        добавляем в кэш и в хранилище
        """
        self.set(key, value, time)
        try:
            self.redis.set(key, value)
        except:
            pass

    def cache_get(self, key):
        """
        пытаемся забрать из кэша, в случае неудачи - пытаемся забрать из хранилища
        """
        value = self.get(key)
        if not value:
            try:
                value = self.redis.get(key)
            except:
                pass
        return value

