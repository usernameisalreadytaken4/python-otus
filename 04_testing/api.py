#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
import logging
import hashlib
import uuid
from functools import wraps
from optparse import OptionParser
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from scoring import get_interests, get_score
from cache import Cache


CLIENTS_INTERESTS = "clients_interests"
ONLINE_SCORE = "online_score"
SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Field(object):
    def __init__(self, label=None, required=True, nullable=False):
        self.label = label
        self.required = required
        self.nullable = nullable

    def __get__(self, instance, cls):
        return instance.__dict__.get(self.label)

    def __set__(self, instance, value):
        self._nullable(value)
        self._required(value)
        self._validate(value)
        if hasattr(self, 'value'):
            instance.__dict__[self.label] = self.value
            self.value = None
        else:
            instance.__dict__[self.label] = value

    def _required(self, value):
        if self.required and value is None:
            raise ValueError('Expected VALUE')

    def _nullable(self, value):
        if self.nullable is False:
            if isinstance(value, int) or isinstance(value, float):
                if value == 0 or value == 0.0:
                    raise ValueError('Value is 0')
            else:
                if len(value) == 0:
                    raise ValueError('Value is 0')

    def _validate(self, value):
        pass


class Descriptor(type):
    def __new__(cls, name, bases, attrs):
        __fields__ = []
        for n, v in attrs.items():
            if isinstance(v, Field):
                v.label = n
                __fields__.append(n)
        attrs.update({'__fields__': __fields__})
        return super(Descriptor, cls).__new__(cls, name, bases, attrs)


class Base(object):

    __metaclass__ = Descriptor

    def validate(self):
        return True

    def load(self, _dict):
        for key in self.__class__.__fields__:
            value = _dict.get(key)
            setattr(self, key, value)


class CharField(Field):

    def __init__(self, value=None, required=True, nullable=False):
        super(CharField, self).__init__(value, required, nullable)

    def _validate(self, value):
        if value:
            if not isinstance(value, str):
                raise ValueError('Wrong name or surname')


class ArgumentsField(Field):
    def __init__(self, value=None, required=True, nullable=False):
        super(ArgumentsField, self).__init__(value, required, nullable)


class EmailField(CharField):
    def __init__(self, value=None, required=True, nullable=False):
        super(EmailField, self).__init__(value, required, nullable)

    def _validate(self, value):
        if value:
            if '@' not in value:
                raise ValueError('wrong email')


class PhoneField(Field):
    def __init__(self, value=None, required=True, nullable=False):
        super(PhoneField, self).__init__(value, required, nullable)

    def _validate(self, value):
        if value:
            if len(str(value)) != 11 or str(value)[0] != '7':
                raise ValueError('wrong phone')


class DateField(Field):
    def __init__(self, value=None, required=True, nullable=False):
        super(DateField, self).__init__(value, required, nullable)

    def _validate(self, value):
        if value:
            if isinstance(value, datetime.datetime):
                return True
            try:
                datetime.datetime.strptime(value, '%d.%m.%Y')
                self.value = datetime.datetime.strptime(value, '%d.%m.%Y')
            except:
                raise ValueError('Incorrect date format')


class BirthDayField(DateField):
    def __init__(self, value=None, required=True, nullable=False):
        super(BirthDayField, self).__init__(value, required, nullable)
        self.cap = datetime.datetime.now() - datetime.timedelta(days=70 * 365)

    def _validate(self, value):
        super(BirthDayField, self)._validate(value)
        if value:
            if datetime.datetime.strptime(value, '%d.%m.%Y') < self.cap:
                raise ValueError('Cap overhead')


class GenderField(Field):
    def __init__(self, value=None, required=True, nullable=False):
        super(GenderField, self).__init__(value, required, nullable)

    def __set__(self, instance, value):
        self._required(value)
        self._nullable(value)
        self._validate(value)
        instance.__dict__[self.label] = value

    def _validate(self, value):
        if value:
            if value not in [0, 1, 2]:
                raise ValueError('Wrong Gender')


class ClientIDsField(Field):
    def __init__(self, value=None, required=True, nullable=False):
        super(ClientIDsField, self).__init__(value, required, nullable)

    def _validate(self, value):
        if value:
            if not isinstance(value, list):
                raise ValueError('Expected List')
            for item in value:
                if not isinstance(item, int):
                    raise ValueError('Expected ID type INT')


class ClientsInterestsRequest(Base):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Base):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def validate(self):
        if (self.first_name and self.last_name)\
                or (self.email and self.phone)\
                or (self.gender in [0, 1, 2] and self.birthday):
            return True
        return False


class MethodRequest(Base):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


BASES_MAP = {
    ONLINE_SCORE: OnlineScoreRequest,
    CLIENTS_INTERESTS: ClientsInterestsRequest
}


ACTION_MAP = {
    ONLINE_SCORE: get_score,
    CLIENTS_INTERESTS: get_interests
}

RESPONSE_MAP = {
    ONLINE_SCORE: 'score',
    CLIENTS_INTERESTS: 'interests'
}


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def validate_request(func):
    @wraps(func)
    def wrapper(request, ctx):
        request_obj = MethodRequest()
        try:
            request_obj.load(request.get('body'))
        except:
            return ERRORS.get(INVALID_REQUEST), INVALID_REQUEST
        if not request_obj.login or not check_auth(request_obj):
            return ERRORS.get(FORBIDDEN), FORBIDDEN
        return func(request_obj, ctx)

    return wrapper


@validate_request
def method_handler(request_obj, ctx):

    arg_obj = BASES_MAP[request_obj.method]()

    store = Cache()
    try:
        arg_obj.load(request_obj.arguments)
    except Exception:
        return ERRORS.get(INVALID_REQUEST), INVALID_REQUEST

    ctx.update({'has': {field: getattr(arg_obj, field) for field, value in arg_obj.__class__.__dict__.items()
           if isinstance(value, Field) and getattr(arg_obj, field) is not None}})

    if arg_obj.validate():
        if request_obj.is_admin:
            result = 42
        else:
            result = ACTION_MAP[request_obj.method](store=store, args_obj=arg_obj)
        response = {RESPONSE_MAP[request_obj.method]: result}
        code = OK
    else:
        return ERRORS.get(INVALID_REQUEST), INVALID_REQUEST

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = Cache()

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception, e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8081)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
