# -*- coding: utf-8 -*-
import pytest
from pytest_mock import mocker

import api


@pytest.fixture()
def mocked_required(mocker):
    """
    :return: ломаем проверку на required
    """
    mocked_required = mocker.patch('api.Field._required')
    mocked_required.return_value = None
    return mocked_required

@pytest.fixture()
def mocked_nullable(mocker):
    """
    :return: ломаем проверку на nullable
    """
    mocked_nullable = mocker.patch('api.Field._nullable')
    mocked_nullable.return_value = None
    return mocked_nullable


class TestFields:

    @pytest.mark.parametrize('requests', [
        {"account": "horns&hoofs"},
        {"login": "h&f"},
        {"method": "online_score"},
        {"arguments": {}},
    ])
    def test_request_fields(self, requests, mocked_required, mocked_nullable):
        obj = api.MethodRequest()
        obj.load(requests)
        assert obj

    @pytest.mark.parametrize('arguments', [
        {"phone": "79175002040"},
        {"email": "stupnikov@otus.ru"},
        {"gender": 1},
        {"birthday": "01.01.2000"},
        {"first_name": "a"},
        {"last_name": "b"},
    ])
    def test_score_fields(self, arguments, mocked_required, mocked_nullable):
        obj_args = api.OnlineScoreRequest()
        obj_args.load(arguments)
        assert obj_args

    @pytest.mark.parametrize('arguments', [
        {"client_ids": [1, 2, 3]},
        {"date": "19.07.2017"},
    ])
    def test_clients_interests_fields(self, arguments, mocked_required, mocked_nullable):
        """
        :param arguments: client_ids - required
        :return:
        """
        obj_args = api.ClientsInterestsRequest()
        obj_args.load(arguments)
        assert obj_args

    @pytest.mark.parametrize('requests', [
        {"account": 12312},
        {"login": 123},
        {"method": 4323},
        {"arguments": 123},
    ])
    def test_invalid_method_fields(self, requests, mocked_required, mocked_nullable):
        try:
            obj = api.MethodRequest()
            obj.load(requests)
            assert False
        except:
            assert True

    @pytest.mark.parametrize('arguments', [
        {"phone": "79175002040"},
        {"gender": -1},
        {"email": 12321321},
        {'first_name': 2},
        {'last_name': []},
        {"birthday": "XXX"},
    ])
    def test_invalid_score_fields(self, arguments, mocked_required, mocked_nullable):
        try:
            obj_args = api.OnlineScoreRequest()
            obj_args.load(arguments)
            assert False
        except:
            assert True

    @pytest.mark.parametrize('arguments', [
        {"client_ids": ''},
        {"date": "XXX"},
    ])
    def test_invalid_interests_fields(self, arguments, mocked_required, mocked_nullable):
        try:
            obj_args = api.ClientsInterestsRequest()
            obj_args.load(arguments)
            assert False
        except:
            assert True