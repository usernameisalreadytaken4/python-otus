# -*- coding: utf-8 -*-
import pytest
import memcache
import redis
from pytest_mock import mocker
from cache import Cache


class MemDict(dict):

    def __init__(self, *args, **kwargs):
        super(MemDict, self).__init__(*args, **kwargs)

    def set(self, key, value, time=None):
        self.update({key: value})


class RedisDict(dict):

    def __init__(self, *args, **kwargs):
        super(RedisDict, self).__init__(*args, **kwargs)

    def set(self, key, value):
        self.update({key: value})



@pytest.fixture()
def mocked_memcache(mocker):
    mocked_memcache = mocker.patch('memcache.Client')
    mocked_memcache.return_value = MemDict()
    return mocked_memcache


@pytest.fixture()
def mocked_redis(mocker):
    mocked_redis = mocker.patch('redis.StrictRedis')
    mocked_redis.return_value = RedisDict()
    return mocked_redis


class TestStore:

    @pytest.mark.parametrize('key,value', [
        ('key1', 'value1'),
        ('key2', 'value2'),
        ('key3', 'value3'),
    ])
    def test_set_get_memcache(self, key, value, mocked_memcache):
        store = Cache()
        store.set(key, value, 10 * 10)
        recv_value = store.get(key)
        assert recv_value == value

    @pytest.mark.parametrize('key,value', [
        ('key1', 'value1'),
        ('key2', 'value2'),
        ('key3', 'value3'),
    ])
    def test_set_get_redis(self, key, value, mocked_redis):
        store = Cache()
        store.cache_set(key, value, 10 * 10)
        recv_value = store.cache_get(key)
        assert recv_value == value