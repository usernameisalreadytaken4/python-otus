import hashlib
import json
import random

interests = ['tv', 'games', 'music', 'pets', 'work', 'walking', 'races']


def get_score(store, args_obj):
    key_parts = [
        args_obj.first_name or "",
        args_obj.last_name or "",
        args_obj.birthday.strftime("%Y%m%d") if args_obj.birthday is not None else "",
    ]
    key = "uid:" + hashlib.md5("".join(key_parts)).hexdigest()
    # try get from cache,
    # fallback to heavy calculation in case of cache miss
    score = store.cache_get(key) or 0
    if score:
        return score
    if args_obj.phone:
        score += 1.5
    if args_obj.email:
        score += 1.5
    if args_obj.birthday and args_obj.gender:
        score += 1.5
    if args_obj.first_name and args_obj.last_name:
        score += 0.5
    # cache for 60 minutes
    store.cache_set(key, score,  60 * 60)
    return score


def get_interests(store, args_obj):
    cids = args_obj.client_ids
    res = {}
    for cid in cids:
        r = store.cache_get("i:%s" % cid)
        if not r:
            r = random.sample(set(interests), 2)
            store.cache_set("i:%s" % cid, json.dumps(r))
        else:
            r = json.loads(r)
        res.update({cid: r})
    return res if res else {}