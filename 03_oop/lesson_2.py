import json


class Field(object):
    def __init__(self, value=None):
        self.value = value

    def __get__(self, instance, cls):
        return self.value

    def __set__(self, instance, value):
        self.value = value
        self._validate()

    def _validate(self):
        pass


class IntegerField(Field):
    def __init__(self, max_len=0, value=None):
        super(IntegerField, self).__init__(value)
        self.max_len = max_len

    def _validate(self):
        if self.value is None:
            return
        if not isinstance(self.value, int):
            raise TypeError('Expected INT')
        if self.max_len != 0 and self.value > self.max_len:
            raise ValueError('too big')


class StringField(Field):
    def __init__(self, max_len=0, value=None):
        super(StringField, self).__init__(value)
        self.max_len = max_len

    def _validate(self):
        if self.value is None:
            return
        if not isinstance(self.value, str):
            raise TypeError('Expected STR')
        if self.max_len != 0 and len(self.value) > self.max_len:
            raise ValueError('too big')


class ORMBase(object):

    def __init__(self, data=None):
        if data:
            data = json.loads(data)
            if not isinstance(data, dict):
                raise ValueError('Invalid JSON')
            for key, val in data.iteritems():
                setattr(self, key, val)

    def to_json(self):
        data = {}
        for key, val in self.__class__.__dict__.items():
            if isinstance(val, Field):
                data[key] = getattr(self, key)
        return json.dumps(data)


#################################

class User(ORMBase):
    id = IntegerField()
    name = StringField(max_len=64)
    surename = StringField(max_len=64)
    height = IntegerField(max_len=250)
    year = IntegerField(max_len=2018)


u = User()

u.name = 'Guido'
u.surename = 'van Rossum'

print 'id=', u.id


# try:
#     # u.name = 'a' * 65
# except ValueError:
#     print 'OK'

print 'JSON=', u.to_json()

js = '{"id": 2, "name": "Vasya", "surename": "Pupkin", "height": 195, "year": 1989}'
u2 = User(js)

print 'id=', u2.id
print 'name=', u2.name


