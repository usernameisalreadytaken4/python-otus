# -*- coding: utf-8 -*-

from functools import wraps


def debug(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print 'there\'s call of', func.__name__
        func(*args, **kwargs)
    return wrapper


def debug_class(cls):
    for key, val in vars(cls).iteritems():
        if callable(val):
            setattr(cls, key, debug(val))
    return cls


class DebugMeta(type):
    def __new__(meta, name, bases, dct):
        cls = super(DebugMeta, meta).__new__(meta, name, bases, dct)
        return debug_class(cls)


class C:
    __metaclass__ = DebugMeta
    def method(self, a):
        print(a)

    def method2(self):
        return 42


class C2(C):
    def second_method(self, a):
        print a


c = C()
c.method(42)
c.method2()

c = C2()
c.second_method(42)
